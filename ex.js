// Bài tập 1

// lấy giá trị khu vực
function diemTheoKhuVuc(khuVuc) {
  if (khuVuc == "A") {
    return 2;
  }
  if (khuVuc == "B") {
    return 1;
  }
  if (khuVuc == "C") {
    return 0.5;
  }
  if (khuVuc == "D") {
    return 0;
  }
}
// lấy giá trị đối tượng
function diemTheoDoiTuong(doiTuong) {
  switch (doiTuong) {
    case "1":
      return 2.5;
    case "2":
      return 2;
    case "3":
      return 1.5;
    case "0":
      return 0;
  }
}

function ketQua() {
  var diemChuan = document.getElementById("txt-diemchuan").value * 1;
  var a = document.getElementById("txt-diem1").value * 1;
  var b = document.getElementById("txt-diem2").value * 1;
  var c = document.getElementById("txt-diem3").value * 1;
  var khuVuc = document.getElementById("khuVuc").value;
  var doiTuong = document.getElementById("doiTuong").value;
  var diemKhuVuc = diemTheoKhuVuc(khuVuc);
  var diemDoiTuong = diemTheoDoiTuong(doiTuong);
  var diemTong = a + b + c + diemKhuVuc + diemDoiTuong;

  if (a != 0 && b != 0 && c != 0) {
    if (diemTong >= diemChuan) {
      document.getElementById(
        "ketQua"
      ).innerText = `Bạn đã đậu. Tổng điểm là ${diemTong}`;
    } else {
      document.getElementById(
        "ketQua"
      ).innerText = `Bạn đã rớt. Tổng điểm là ${diemTong}`;
    }
  } else {
    document.getElementById("ketQua").innerText =
      "Bạn đã bị rớt do có môn bị 0 điểm";
  }
}
// Kết thúc bài tập 1

// Bài tập 2
function tinhTien() {
  var hoTen = document.getElementById("hoTen1").value;
  var kwDien = document.getElementById("kwDien").value * 1;
  if (kwDien <= 50) {
    tongTien = kwDien * 500;
  } else if (kwDien <= 100) {
    tongTien = 50 * 500 + (kwDien - 50) * 650;
  } else if (kwDien <= 200) {
    tongTien = 50 * 500 + 50 * 650 + (kwDien - 100) * 850;
  } else if (kwDien <= 350) {
    tongTien = 50 * 500 + 50 * 650 + 100 * 850 + (kwDien - 200) * 1100;
  } else {
    tongTien =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (kwDien - 350) * 1300;
  }
  document.getElementById(
    "result"
  ).innerText = `Họ tên: ${hoTen}, Tổng tiền điện: ${new Intl.NumberFormat().format(
    tongTien
  )} VNĐ`;
}
// Kết thúc bài tập 2

// Bài tập 3
function tinhTienThue() {
  var hoTen = document.getElementById("hoTen2").value;
  var thuNhap = document.getElementById("thuNhap").value * 1;
  var soNguoiPhuThuoc = document.getElementById("soNguoiphuthuoc").value * 1;
  var thuNhapChiuThue = thuNhap - 4e6 - soNguoiPhuThuoc * 1600000;
  if (thuNhapChiuThue <= 60e6) {
    tienThue = thuNhapChiuThue * 0.05;
  } else if (thuNhapChiuThue <= 120e6) {
    tienThue = thuNhapChiuThue * 0.1;
  } else if (thuNhapChiuThue <= 210e6) {
    tienThue = thuNhapChiuThue * 0.15;
  } else if (thuNhapChiuThue <= 384e6) {
    tienThue = thuNhapChiuThue * 0.2;
  } else if (thuNhapChiuThue <= 624e6) {
    tienThue = thuNhapChiuThue * 0.25;
  } else if (thuNhapChiuThue <= 960e6) {
    tienThue = thuNhapChiuThue * 0.3;
  } else {
    tienThue = thuNhapChiuThue * 0.35;
  }
  document.getElementById(
    "tienThue"
  ).innerText = `Họ tên: ${hoTen}, Tiền thuế thu nhập: ${new Intl.NumberFormat().format(
    tienThue
  )} VNĐ `;
}
// Kết thúc bài tập 3

// Bài tập 4
// onchange
function doanhNghiep() {
  var value = document.getElementById("khachHang").value;
  if (value == "2") {
    document.getElementById("soKetNoi").style.display = "block";
  } else {
    document.getElementById("soKetNoi").style.display = "none";
  }
}
function tinhTienCap() {
  var maKhachHang = document.getElementById("maKhachHang").value;
  var soKenh = document.getElementById("soKenh").value * 1;
  var soKetNoi = document.getElementById("soKetNoi").value * 1;
  var khachHang = document.getElementById("khachHang").value;
  if (khachHang == "1") {
    tienCap = 4.5 + 20.5 + 7.5 * soKenh;
  } else {
    if (soKetNoi <= 10) {
      tienCap = 15 + 75 + 50 * soKenh;
    } else {
      tienCap = 15 + 75 + (soKetNoi - 10) * 5 + 50 * soKenh;
    }
  }
  document.getElementById(
    "tienCap"
  ).innerText = `Mã khách hàng: ${maKhachHang}, Tiền cáp: $${new Intl.NumberFormat().format(
    tienCap
  )}`;
}
